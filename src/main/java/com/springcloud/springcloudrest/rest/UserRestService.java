package com.springcloud.springcloudrest.rest;

import com.springcloud.springcloudrest.model.User;
import com.springcloud.springcloudrest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestService {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> getAllUsers(){
        return this.userService.listAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public void addUser(){
        this.userService.addUser();
    }
}
