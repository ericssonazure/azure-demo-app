package com.springcloud.springcloudrest.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ServiceExample {

    private static Logger log = LoggerFactory.getLogger(ServiceExample.class);


    String str = "Hello World";

    @RequestMapping(value = "/example")
    public String example() {

        String result = "{Empty Value}";

        if(str.equals(null)){

            log.error("PublicRestService - Called with errors property rest.service.cloud.config.example is empty");

        }else{
            log.info("PublicRestService - Called with this property: (rest.service.cloud.config.example: "+str+")");
            result = str;
        }
        return result;
    }
}
