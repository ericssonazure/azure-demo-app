package com.springcloud.springcloudrest.service;

import com.springcloud.springcloudrest.model.User;

import java.util.List;

public interface UserService {

    List<User> listAll();

    void addUser();
}
