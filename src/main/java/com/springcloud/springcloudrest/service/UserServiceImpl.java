package com.springcloud.springcloudrest.service;

import com.springcloud.springcloudrest.model.User;
import com.springcloud.springcloudrest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> listAll() {
        return this.userRepository.findAll();
    }

    @Override
    public void addUser() {
        User user=new User("1","angel", "miralles");

        this.userRepository.save(user);
    }
}
