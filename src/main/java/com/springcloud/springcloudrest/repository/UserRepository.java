package com.springcloud.springcloudrest.repository;

import com.microsoft.azure.spring.data.documentdb.repository.DocumentDbRepository;
import com.springcloud.springcloudrest.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends DocumentDbRepository<User, String> {}
